<?php

Route::get('/', function () {
    return view('frontend.home.index');
});

Route::get('/portfolio', 'IndexController@portfolio');
