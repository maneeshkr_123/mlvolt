<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function index(){
        return view('frontend.index.index');
    }

    public function portfolio(){
        return view('frontend.portfolio.index');
    }
}
